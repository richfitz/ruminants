library(ape)

## Basic data loading and cleaning.
load.trees <- function() {
  trees <- readRDS("data/ruminants-trees.rds")
  trees
}

load.trait.data <- function() {
  ## Load the trait data from `data/ruminants-traits.csv`.
  d <- read.csv("data/ruminants-traits.csv", as.is=TRUE)

  ## There is a typo in the trait data to fix:
  d$tip.label[d$tip.label == "Moschus_berezovskii"] <- "Moschus_berezovski"

  d <- d[match(load.trees()[[1]]$tip.label, d$tip.label),]
  states <- structure(d$diet+1, names=d$tip.label)
  attr(states, "levels") <- c("B", "M", "G")
  states
}

load.climate <- function() {
  climate <- read.csv("data/climate.csv")
  climate <- climate[!is.na(climate$d18O),c("Age", "d18O")]
  climate
}

load.climate.spline <- function(climate=load.climate()) {
  ## First, fit a *smoothing* spline through the climate data.
  fit.ss <- smooth.spline(climate$d18O ~ climate$Age, spar=.8)

  ## We need x/y points to regenerate this spline when running the
  ## time-varying MuSSE analyses, as a list of t / y points.  These
  ## need to span the range of observed times over all trees, plus a
  ## little extra.  The deepest tree goes back 57 My, while the
  ## climate data goes back 70 My, so this is not a problem.  Generate
  ## 1,000 evenly spaced time points between 58 Mya and the present
  ## (0), and compute the smoothed d18O value at these points.  The
  ## list `spline.data` can be passed in to `make.musse.t`.
  t <- seq(0, 58, length=1000)
  list(t=t, y=predict(fit.ss, t)$y)
}

make.lik <- function(idx) {
  phy <- load.trees()[[idx]]
  states <- load.trait.data()
  spline.data <- load.climate.spline()

  ## First, consider the constant time models (suffix .m for musse).
  ## Models can be full (all parameters):
  lik.m.full <- make.musse(phy, states, 3)
  lik.t.full <- make.musse.t(phy, states, 3,
                             rep(c("spline.t", "constant.t"), c(3, 9)),
                             spline.data=spline.data)
  ## ... and flex (flexible diet evolution, disallowing B->G and G->B
  ## transitions).
  lik.m.flex <- constrain(lik.m.full, q13 ~ 0, q31 ~ 0)
  lik.t.flex <- constrain(lik.t.full, q13 ~ 0, q31 ~ 0)

  list(m.full=lik.m.full, m.flex=lik.m.flex,
       t.full=lik.t.full, t.flex=lik.t.flex)
}


filename.ml <- function(idx)
  sprintf("output/ml-%d.rds", idx)

filename.mcmc <- function(idx)
  sprintf("output/mcmc-%d.rds", idx)

starting.point <- function(idx)
  starting.point.musse(load.trees()[[idx]], 3)

get.idx.from.argument <- function() {
  args <- commandArgs(TRUE)
  if (length(args) != 1)
    stop("Expected exactly one argument")
  oo <- options(warn=2)
  on.exit(options(oo))
  as.integer(args[[1]])
}
