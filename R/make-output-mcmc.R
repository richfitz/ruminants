#!/usr/bin/env Rscript
library(diversitree, quietly=TRUE)
source("R/common.R")
source("R/fits.R")
dir.create("output", FALSE)
ruminants.mcmc(get.idx.from.argument())
