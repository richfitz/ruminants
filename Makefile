NTREES := 100
TREES := $(shell seq 1 ${NTREES})
JOBS_ML   := $(addsuffix .rds, $(addprefix output/ml-,   ${TREES}))
JOBS_MCMC := $(addsuffix .rds, $(addprefix output/mcmc-, ${TREES}))

all: ruminants simulations

ruminants: ruminants-ml ruminants-mcmc
ruminants-ml: ${JOBS_ML}
ruminants-mcmc: ${JOBS_MCMC}
simulations: simulations/ml.rds

data/ruminants-trees.rds:
	Rscript R/make-ruminants-trees.rds.R

output/ml-%.rds: data/ruminants-trees.rds
	Rscript R/make-output-ml.R $*
output/mcmc-%.rds: output/ml-%.rds
	Rscript R/make-output-mcmc.R $*

simulations/trees.rds:
	Rscript R/make-simulations-trees.R
simulations/ml.rds: simulations/trees.rds
	Rscript R/make-simulations-ml.R

.PHONY: all ruminants simulations ruminants-ml ruminants-mcmc
